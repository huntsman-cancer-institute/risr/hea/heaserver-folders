#!/usr/bin/env python3

from heaserver.folder import service
from heaserver.service import swaggerui
from integrationtests.heaserver.folderintegrationtest.foldertestcase import db_values_expected
import logging

logging.basicConfig(level=logging.DEBUG)


if __name__ == '__main__':
    swaggerui.run('heaserver-folders', db_values_expected, service,
                  [('/folders/{folder_id}/items/{id}', service.get_item)], 'heaserver-registry:latest')
