-e .
chardet~=3.0 # Transitive dependency. We need to pin it to version 3.* because one of our dependencies does not.
pytest~=5.4.2
twine~=3.1.1
wheel~=0.34.2
git+https://github.com/arpost/testcontainers-python@1106e3bd#testcontainers
mypy==0.910
pip-licenses~=3.4.0
aiohttp[speedups]~=3.6.2
aiohttp-swagger3~=0.6.0
heaserver-registry==1.0.0a15
